# Process Management #

Here we'll look at how to do various things with running programs or processes.

# Our Test Job #

`./long_running.py` will run for 2 minutes, and will output any arguments passed to it once a second. We're going to use it as an example of how to stop programs as well as use the following builtins: `bg`, `fg`, `nohup`, `kill` and `killall`

# `bg` and `fg` #

`bg` and `fg` are used to move an already running process into the background or a backgrounded process into the foreground. This can be handy when you have a single terminal open, you start something that is going to take a long time, and want to keep working without having to open a new terminal window.

## How to Background a Job You Just Started ##

`bg` will cause whatever you are currently doing to go to the background. To be able to use it you need to first pause whatever program is running in the foreground at the moment, which you can do with control-z. Note that control-z does not mean undo. There is no undo.

You can now list all your jobs that have been backgrounded using `jobs`. If there is only one you can bring it back to interactive mode by running `fg` to foreground the process.

## How to Background a Job Immediately ##

If you know *before* you start a program that you don't want to see it run, or interact with it, then you've got a couple options. In Bash you can immediately background a process by ending the invocation with an apersand `&`. Like this:

```shell
./long_running.py args go here &
```

You'll notice that you're going to still get output from that backgrounded program which is annoying. So, let's make our invocation a bit more complex to log any output to a file we'll call log.txt.

```shell
./long_running.py args go here > log.txt 2>&1 &
```

Silence, wonderful silence. Probably? How do we know it's working? Well there are a few options. The first is to immediately check that 'log.txt' exists, and if there's anything in it. If there's nothing there, we can also check the output of `jobs` to see if the process is there.

## How to Prevent Hangups ##

When you disconnect from a terminal session, by closing the window on your terminal for example, the system sends a signal to anything that might be attached to the terminal which typically will shut them down. If you've got a long running non-interactive process which you want to remain running even after you disconnect you want `nohup`. `nohup` works like this:

    nohup ./long_running.py args go here &

Note that you still need the ampersand on the end, but if all you want to do is capture the output, and you don't care about what the file is named `nohup` will do that for you. `nohup` will always redirect output into a file called 'nohup.out'. If you want your output in specifically named files you'll need handle the redirection yourself as before.

## How To Prevent Hangups With An Interactive Process: `tmux` ##

There are two programs I use that I need to have running, that are interactive, but not much, and I don't want them to be closed if I forget they are open and close a Terminal session. To keep them running and interactive I use `tmux`.

For a contrived example let's open the text editor `nano` using `tmux`:

    tmux new -s nano_session

    # Start nano and enter some text
    nano
    I am content, blah blah blah
    # Detach from the tmux session
    'control-b'
    ':detach'

Optionally at this point you can close your terminal. Of course `tmux` isn't magic, if you turn your computer off, or restart, you'll lose the session.

    # List tmux sessions
    tmux ls
    # 
    tmux attach -t nano_session
    
    # Exit nano
    'control x'  # Exit
    n  # Do not save
    # Stop the tmux session
    'control-d'

## Figuring Out What The System is Doing Using `ps` and `top` ##

A common question I get is why is `the_thing_i_am_doing` slow? There are lot of things that could be going on, but I'll often first check the system's CPU usage and available RAM, since if either of those are maxed out then they're likely to be the problem.

An easy way to get a sense of how busy the system is is to use `top`. Look at the columns for `USER`, `CPU`, `MEM%`, and `COMMAND` for easy answers. Press 'q' or 'control-c' to exit.

If you find you need to get a quick snapshort of all running processes you can use `ps`. A common pattern is to pipe the output of `ps` to `grep` and look for something in particular. Try this:

     ./launch_many.sh && sleep 2 && ps aux | grep 'banana'

 `./launch_many.sh` will start 3 `./long_running.py` with different arguments including 'apple', 'banana', and 'cherry'. By running three different versions of the same program we can see one way to identify them after launch.