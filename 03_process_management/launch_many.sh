#!/bin/bash

echo 'Starting 3 long_running.py with redirection to log_*.txt'

nohup ./long_running.py log_1 apple > log_1.txt &
nohup ./long_running.py log_2 banana > log_2.txt &
nohup ./long_running.py log_3 cherry > log_3.txt &

echo 'Finished starting long running processes'
