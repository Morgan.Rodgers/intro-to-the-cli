#!/usr/bin/env python
import os

pid = str(os.getpid())

try:
    while True:
        user_input = raw_input(
            'Enter the current pid {pid} to exit: '.format(
                pid=pid
            )
        ).strip()

        if user_input == pid:
            print('Exiting.')
            break
except (KeyboardInterrupt, Exception) as e:
    print('\nExiting due to exception')