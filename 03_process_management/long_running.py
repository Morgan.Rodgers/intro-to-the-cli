#!/usr/bin/env python
from threading import Event
import sys


def main():
    seconds = 1
    number_of_cycles = 120

    for i in range(number_of_cycles):
        print(sys.argv)
        Event().wait(seconds)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Received SIGINT')
    except Exception as e:
        print('Caught exception of type {0} with value {1}'.format(
            type(e),
            str(e)
        ))
