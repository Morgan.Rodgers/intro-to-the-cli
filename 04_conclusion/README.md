## Customizing ##

Often CLI users will write short programs, and aliases to aid them with common tasks. The place to store these customizations varies based on your flavor of UNIX and preferred shell. For the Bash shell which we've been using the file to edit is `~/.bashrc`. For macOS users the file is `~/.bash_profile`.

A common practice is to extend the `PATH` variable to include a 'bin' directory in your home.

```shell
export PATH="$PATH:~/bin"
```

## If You Are In The CLI and Want a GUI ##

Since Babbage has a full desktop environment installed you can also launch GUI tools from the command line.

Directories are opened using `nautilus`. Images may be viewed using `eog` which is the Eye of Gnome. `firefox` can do websites as well as images, and PDFs. `libreoffice` can handle most of Microsoft Office, but is likely to break formatting if used for editing.

## In Conculsion ##

We've looked at how to list and traverse the file system, how to find files by name and content, how to manage processes, and how to manage files. There are many other topics that touch on how to use UNIX without a graphic interface, but hopefully this will give you a place and some tools to start with.

Some resources for more information include your colleagues, the [UNIX StackExchange](https://unix.stackexchange.com), the UNIX section of [Indiana State's Knowledge Base](https://kb.iu.edu/d/apek), `man` pages, or if you come across a shell statement that you'd like parsed out for you [Explain Shell](https://explainshell.com) is an option.

### P.S. ###

One of my favorite things about using the shell is mass copy and paste. I recently started porting the IAM's code from Python 2.6 to Python 3. This meant among other changes that I needed to change the shebang headers (`#!/usr/bin/env python`) to point to Python3.

Instead of editing each file manually I used a combination of `find` and `perl` specifically the `perl -pi -e` idiom.

```shell
perl -pi -e 's[PATTERN][REPLACEMENT]g
find . -type f -name '*.pyc' -exec perl -pi -e 's[#!/usr/bin/env python][#!/usr/bin/env python3]' {} \;
```
### P.P.S. ###

If you find yourself stuck in Vim, for example because it's configured as Git's default editor. You can exit with the key combination of `escape` - `:` - `wq` - `enter` if you want to save your changes, or `escape` - `:` - `q!` - `enter` if you prefer to quit without saving.