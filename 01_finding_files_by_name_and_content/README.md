# Finding Files By Name and Content (and Meta Data) #

An exercise in finding files by their name or content.

## Build the haystack ##

First run `./make_random_file_tree.py` to build the test directory: haystack. It'll be full of files and directories, all with random single word names.

    ./make_random_file_tree.py

Three of those files are special: one isn't random and is named needle, another contains a short TODO list, and the last contains some phone numbers in the format `[0-9]{3}-[0-9]{3}-[0-9]{4}` or if you're not familiar with regular expressions something like 123-456-7890.

## Finding Files By Name ##

To find files by name you can use the utility `find`. `find` has a lot of options, but for the moment lets just use it to list the contents of 'haystack:

    find haystack

If you look carefully through that list you can find 'needle_file.txt', but there's a better way.  Instead use `find` to search directory for a file with a known name of 'needle_file.txt'.

    find haystack -name 'needle_file.txt'

If we weren't sure of the exact name of the file, but knew that it contained 'needle' in it somewhere, we could instead write:

    find haystack -name '*needle*' # those two astericks act as a fuzzy match allowing find to locate any file where needle is in the name

## Finding Files By Content ##

But what about searching for an unknown file by content? Our simple example here is that we know that the string 'TODO' exists somewhere in the file. To search file contents we'll use a program called `grep`. Like `find` grep is a powerful tool, and if you want to know more about the many options available for `grep` or `find` or most UNIX tools you can use the program `man` to pull up the manual page for each tool. Invoking `man` is simple:

    man grep

To find a file containing the string 'TODO' invoke `grep` like this:

    grep -r TODO haystack

This tells `grep` to act recursively over a directory named 'haystack' looking for a literal value of 'TODO'. If this is the first time that you've heard the word 'recursive' you can think of it-for the moment-as a way to operate over nested directories. That's not the real definition, but since many UNIX tools use -r or -R flags to operate on directories it's a useful placeholder for now.

## Finding Files By Age, Size, etc ##

`find` can also filter a file tree by file access time, file modification time, file size and other attributes.

## Find Can Be Used to Delete Files ##

If you need to delete some specific files that are spread across multiple directories try using `find` once you've got your statement showing only the files you want removed-and remembering the dire warnings about deleting things from the command line-just pass the flag `-delete`.