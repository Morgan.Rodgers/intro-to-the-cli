#!/usr/bin/env python
from contextlib import contextmanager
from time import time
import gzip
import os
import random
import shutil


@contextmanager
def cd(path):
    cwd = os.getcwd()
    os.chdir(path)
    yield
    os.chdir(cwd)


def make_word_list():
    input_file = gzip.open('words.gz')
    content = input_file.read().strip()
    input_file.close()

    return map(
        lambda s: s.lower(),
        content.split('\n')
    )


def cleanup_prior_attempts():
    try:
        shutil.rmtree('haystack')
    except OSError:
        pass

    try:
        os.unlink('ANSWERS')
    except OSError:
        pass


def get_fake_utime():
    now = time()
    atime = now - random.randrange(1, one_month * 12)
    mtime = now - random.randrange(1, one_month * 12)

    if mtime > atime:
        mtime = atime

    return (atime, mtime)


one_month = 3600 * 24 * 32
def make_random_file(word_list):
    file_name = '{0}.txt'.format(
        random.choice(word_list)
    )
    index_start = random.randrange(0, len(word_list) / 2)
    index_end = index_start + random.randrange(
        1, random.randrange(6, 20)
    )

    with open(file_name, 'w') as output_file:
        output_file.write(
            ' '.join(
                word_list[index_start: index_end]
            )
        )


    

    os.utime(
        file_name,
        get_fake_utime()
    )

    return file_name


max_depth = 5
def _make_haystack(depth, file_paths, word_list):
    cwd = os.getcwd()
    if depth == max_depth:
        for i in range(random.randrange(1, 5)):
            file_name = make_random_file(word_list)
            file_paths += [os.path.join(cwd, file_name)]
        depth -= 1
        return

    depth += 1

    new_dirs = []
    for i in range(random.randrange(2, 5)):
        new_dirs += [random.choice(word_list)]

    for new_dir in new_dirs:
        os.mkdir(new_dir)
        with cd(new_dir):
            _make_haystack(depth, file_paths, word_list)


def make_haystack():
    cleanup_prior_attempts()

    current_depth = 1
    file_paths = []
    word_list = make_word_list()
    random.shuffle(word_list)

    os.mkdir('haystack')
    with cd('haystack'):
        _make_haystack(current_depth, file_paths, word_list)

    return sorted(file_paths)


def make_content_phone_numbers(file_name):
    with open(file_name, 'w') as output_file:
        output_file.write('\n'.join([
            '240-555-5555',
            '802-550-5555',
            '614-550-5555',
        ]))


def make_content_todo(file_name):
    with open(file_name, 'w') as output_file:
        output_file.write('\n'.join([
            'TODO: make lunch'
        ]))


def make_name(file_name):
    with open(file_name, 'w') as output_file:
        output_file.write('Congrats you found it!')


def make_needle_files(file_list):
    needle_path = os.path.dirname(file_list[0])
    needle_file = os.path.join(needle_path, 'needle_file.txt')
    
    make_name(needle_file)
    make_content_todo(file_list[1])
    make_content_phone_numbers(file_list[2])


def make_answer_file(file_list):
    with open('ANSWERS', 'w') as output_file:
        output_file.write('\n'.join([
            'Needle: {0}'.format(
                os.path.join(os.path.dirname(file_list[0]), 'needle_file.txt')
            ),
            'Todo: {0}'.format(file_list[1]), 
            'Phone: {0}'.format(file_list[2]),
            ''
        ]))


def main():
    random.seed(1)

    file_paths = make_haystack()
    random.shuffle(file_paths)
    make_needle_files(file_paths)
    make_answer_file(file_paths)


if __name__ == '__main__':
    main()
