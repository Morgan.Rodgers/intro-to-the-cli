# File Management #

Here we'll learn how to copy, move, delete files, and create and extract a few types of archives.

## An Immediate Aside: Globbing ##

Many times users will want to act on groups of files. In the shell a shortcut for fuzzy matching all the files and directories is the glob operator '*'. Globbing is a quick way to grab files, but it has limits. For better or worse globbing does not grab dot-files and dot-directories, which is how UNIX hides files. How you should use globs varies. With `find` you'll (probably) want to quote the glob. With `ls` you'll typically not need to quote the glob. Since globbing is not explicit use caution when writing a command that modifies or deletes files.

## Copying Files and Directories ##

Copy files using the `cp` command. Use the `-r` flag to copy directories.

## Move (and Rename) Files and Directories ##

Moving and renaming are both done using the `mv` command.

## Deleting Files - Caveat Utilitor (Let the user beware) ##

Deleting stuff is done using the `rm` command. Use caution with `rm` for two reasons: first there is no undo. In general on a UNIX system if you delete thing it is gone forever. At UVM ETS managed systems often (always?) have backups, but in general it pays to be careful. The other issue is that `rm` can delete things faster than the user can react, and may do so silently which can lead to disasters.

> [Toy Story 2 got deleted by accident once because of `rm -rf`](https://thenextweb.com/media/2012/05/21/how-pixars-toy-story-2-was-deleted-twice-once-by-technology-and-again-for-its-own-good/)

For some examples first run `./make_junk_files.py`. That will fill the current directory with a number of 'small_junk_file_*txt' files as well as a directory named 'useless_directory' full of files as well. You can delete those files with `rm` like so:

```shell
rm small_junk_file*  # Notice no quotes around the glob
rm -rf useless_directory
```

For reference you can use `find` for deleting the files:

```shell
find . -maxdepth 1 -type f -name 'small_junk_file*' -delete
```

To delete a directory first its contents must be deleted, so with `find` you would first delete the contents, and then the directories:

```shell
find useless_directory -type f -delete && find useless_directory -type d -delete
```

## Copying Files To And From Remote Systems ##

If you've got SSH access to another UNIX computer you can move files to and from using `scp`.

```shell
# PUT a file in $USER's HOME on the remote
scp README $USER@epscor-pascal.uvm.edu:~/

# Check on the file
ssh $USER@epscor-pascal.uvm.edu ls -ltr

# GET the regular file README from the remote, and rename it PASCAL_README
scp $USER@epscor-pascal.uvm.edu:~/README PASCAL_README

# Check that the file is back
ls -ltr

# Diff the files to prove that they're really the same
diff PASCAL_README README  # Should be blank
```

## Creating Compressed File Archives: `tar` and `zip` ##

Most file archiving on UNIX systems is done using the `tar` command, but sometimes for interoperability with Windows `zip` may be installed as well.

To create an archive compressed named 'useless_directory.tar.gz' with `gzip` from the directory 'useless_directory' with `tar` run:

```shell
tar -czf useless_directory.tar.gz useless_directory
```

To extract that same directory:

```shell
tar -xzf useless_directory.tar.gz
```

For an explanation either `man tar` or have a look at [ExplainShell](https://explainshell.com/explain?cmd=tar+-xzf+useless_directory.tar.gz).

Use `zip` on a directory like this:

```shell
zip -r useless_directory.zip useless_directory
```

And `unzip`:

```shell
unzip useless_directory.zip
```
