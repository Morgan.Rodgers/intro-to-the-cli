#!/usr/bin/env python
import os
import contextlib


@contextlib.contextmanager
def cd(path):
    cwd = os.getcwd()
    os.chdir(path)
    yield
    os.chdir(cwd)


def make_junk_files(root):
    with cd(root):
        for i in range(16):
            file_name = 'small_junk_file_{0}.txt'.format(str(i).zfill(2))
            with open(file_name, 'w') as output_file:
                output_file.write('0' * 256)


def make_junk_dir():
    dir_name = 'useless_directory'
    try:
        os.mkdir(dir_name)
    except:
        pass
    make_junk_files(dir_name)


def main():
    make_junk_files('.')
    make_junk_dir()


if __name__ == '__main__':
    main()
