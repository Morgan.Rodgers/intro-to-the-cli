# Welcome to the UNIX Command Line #

We're going to cover some of the basics of using UNIX from the command line or shell. Specifically we're going to be using Bash, which is the default login shell for RHEL, Ubuntu, Fedora, macOS and other common UNIX and Linux distributions. But the tools that we're going to look at are part of the core utilities for almost every *nix distribution.

Specifically we're going to cover:
* Navigating the file system
* Finding files
* Process management
* Copying/moving/deleting files, and how to create and extract archives

> Note to self. Use Firefox for showing webpages on Windows and not Chrome. Set the font size / zoom to 150%.
