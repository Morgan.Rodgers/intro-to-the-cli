# Navigating and Viewing the File System #

Use the commands `cd` and `ls` to move around the file system and list the contents of directories.

To change directory use the command `cd`. You may use absolute paths from the root `/` or paths relative to the current working directory. The current directory can be referenced as `.`, while the directory up one level is `..`. You can also use the special variable `~` which references `HOME` directories. By default `~` references your `HOME`, but it can also be used to reference other people's `HOME`s by using it like this:

    ls merodger~

To list the contents of a directory use `ls`. To get more information including modification time and file size use:

    ls -l

For more information, sorted by modification date, with human readable sizes and also showing hidden files:

    ls -lath

By default `ls` operates on the current directory, but any directory may be given:

    ls /
